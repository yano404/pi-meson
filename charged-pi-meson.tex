\section{$\pi^{\pm}$メソンによる核力}\label{sec:charged_pi}
$\pi^0$メソンの議論をもとに荷電メソン$\pi^{\pm}$による核力について考えていく.
\ref{sec:QFT}節で見たように, 荷電スピン0ボソンの対応する場は複素スカラー場である.

$\pi^{+}$メソン, $\pi^{-}$メソンの生成, 消滅演算子を$\hat{b}_k^{\dagger}$, $\hat{b}_k$, $\hat{c}_k^{\dagger}$, $\hat{c}_k$とすると, $\pi^{\pm}$のスカラー場$\phi$は
\begin{align}
  \begin{split}
    \phi
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    \hat{b}_k \exp(i\bm{k}\cdot\bm{x})
    + \hat{c}_{-k}^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
    )
    \quad (\n \to \p に対応) \\
    \phi^{\dagger}
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    \hat{b}_k^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
    + \hat{c}_{-k} \exp(i\bm{k}\cdot\bm{x})
    )
    \quad (\p \to \n に対応)
  \end{split}
  \label{eq:phi_pipm}
\end{align}
と表せる.
図\ref{fig:pi-plus-diagram}に示すように, $\phi$の第1項$\hat{b}_k\exp(i\bm{k}\cdot\bm{r})$は中性子が$\pi^{+}$メソンを吸収して, 陽子になることに対応している.
$\exp(i\bm{k}\cdot\bm{r})$は核子の運動量を$\hbar k$だけ増やすことに対応している.
$\phi$の第2項$\hat{c}_{-k}^{\dagger}\exp(-i\bm{k}\cdot\bm{r})$は中性子が$\pi^{-}$メソンを放出して陽子になることを表し, $\exp(-i\bm{k}\cdot\bm{r})$は核子の運動量を$\hbar k$だけ下げることを意味している.
図\ref{fig:pi-minus-diagram}は第2項をダイアグラムに示したものである.
$\phi^{\dagger}$は$\phi$とは逆に, $\pi^{+}$メソンの放出と$\pi^{-}$メソンの吸収に対応している.
つまり, $\phi$は$\n\to\p$, $\phi^{\dagger}$は$\p\to\n$の遷移に対応している.

\begin{figure}[tbp]
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/pi-plus-diagram.png}
      \caption{
        $\n$による$\pi^{+}$メソンの吸収.
        $\hat{b}_k\exp(i\bm{k}\cdot\bm{r})$に対応.
      }
      \label{fig:pi-plus-diagram}
    \end{center}
  \end{minipage}
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/pi-minus-diagram.png}
      \caption{
        $\n$による$\pi^{-}$メソンの放出.
        $\hat{c}_{-k}^{\dagger}\exp(-i\bm{k}\cdot\bm{r})$に対応.
      }
      \label{fig:pi-minus-diagram}
    \end{center}
  \end{minipage}
\end{figure}

次に, $\pi^0$メソンのときと同じように, 荷電メソン場と核子の相互作用$\Ham_{\pi^{\pm}N}^{\prime}$を考える.
$\Ham_{\pi^{\pm}N}^{\prime}$を得るために, まず最初に思いつくのが$\pi^0$メソンの$\Ham_{\pi^0N}^{\prime}=\Ham_{\pi^0N}^{\prime(2)}$を複素スカラー場に拡張してみることである.
\eqref{eq:Ham_pi0_N_2}を複素スカラー場に拡張してみると,
\begin{align}
  \frac{f}{m_\pi} \sum_{n} \qty[
    \bm{\sigma}_n \cdot \grad\phi
    + \bm{\sigma}_n \cdot \grad\phi^{\dagger}
  ]
  \label{eq:H_pi0N_extended}
\end{align}
が得られるが, これでは, 全ての核子に対して, $\pi^{\pm}$メソンの吸収, 放出が可能な形になってしまっており,
\begin{itemize}
  \item 陽子は$\pi^{-}$を放出できない.
  \item 中性子は$\pi^{+}$を放出できない.
\end{itemize}
という電荷の保存則が取り入れられていない.
この電荷の保存則を取り入れるために, アイソスピンという力学変数を導入する.

\begin{tcolorbox}[
    title=アイソスピン,
    breakable,
    colback=black!2!white
  ]
  陽子, 中性子の質量$m_\p$, $m_\n$は,
  \begin{align}
    m_\p
    = \SI{938.3}{MeV}, \quad
    m_\n
    = \SI{939.6}{MeV} .
  \end{align}
  とほとんど等しい.
  さらに, $\p$-$\p$, $\p$-$\n$, $\n$-$\n$間で核力も等しい(荷電独立性).
  このように陽子, 中性子は電荷以外の性質が非常に似ていることから, 陽子, 中性子を核子の状態と捉えることができる.
  つまり, 核子が陽子状態にあるとき, 電荷は$+e$, 中性子状態にあるとき, 電荷は$0$と考えるわけである.
  核子の電荷の状態は$+e$か$0$かしか存在せず, 力学変量(演算子)の固有状態と捉えることができる.
  これはスピン($\ket{\uparrow}$ or $\ket{\downarrow}$)と非常によく似ている.
  実際, スピンの演算子を使って, 陽子, 中性子状態を記述することができて, この物理量をアイソスピンと呼ぶ.

  スピンベクトルと同じようにアイソスピン$\bm{t}$を
  \begin{align}
    \bm{t}
    = \frac{1}{2}\bm{\tau}
  \end{align}
  と定義する.
  $\bm{\tau}$は, Pauli行列と同じである.
  \begin{align}
    \tau_x
    = \mqty(
    0 & 1  \\
    1 & 0
    ),
    \tau_y
    = \mqty(
    0 & -i \\
    i & 0
    ),
    \tau_z
    = \mqty(
    1 & 0  \\
    0 & -1
    ).
  \end{align}
  核子の電荷演算子を
  \begin{align}
    e \frac{1-\tau_z}{2}
    = e \qty(
    \frac{1}{2}
    - t_z
    )
  \end{align}
  と定義すると, $\tau_z = 1$を中性子状態, $\tau_z = -1$を陽子状態と定義できる.
  つまり, スピンの$\ket{\uparrow}$を中性子に, $\ket{\downarrow}$を陽子に対応させるということである.
  アイソスピンの固有状態を$\ket{\n}$, $\ket{\p}$と書くと,
  \begin{align}
    t_z \ket{\n}
    = \frac{1}{2} \tau_z \ket{\n}
    = \frac{1}{2} \ket{\n}, \quad
    t_z \ket{\p}
    = \frac{1}{2} \tau_z \ket{\p}
    = - \frac{1}{2} \ket{\p} .
  \end{align}
  となる.
  スピンのときと同じように昇降演算子$\tau^{(\pm)}$:
  \begin{align}
    \tau^{(\pm)}
    = \frac{1}{2} \qty(
    \tau_x \pm i\tau_y
    )
  \end{align}
  を定義することで, 状態を遷移させることができる.
  $\tau^{(\pm)}$の作用は次のようである.
  \begin{align}
    \tau^{(+)} \ket{\p} = \ket{\n}, \quad
    \tau^{(+)} \ket{\n} = 0, \quad
    \tau^{(-)} \ket{\n} = \ket{\p}, \quad
    \tau^{(-)} \ket{\p} = 0 .
  \end{align}
\end{tcolorbox}

アイソスピンを取り込んで, 正しい形の相互作用$\Ham_{\pi^{\pm}N}^{\prime}$を考える.
図\ref{fig:pi-plus-diagram}, \ref{fig:pi-minus-diagram}で表しているように, \eqref{eq:phi_pipm}の$\phi$は$\n\to\p$, $\phi^{\dagger}$は$\p\to\n$の遷移に対応している.
これを考慮に入れて, \eqref{eq:H_pi0N_extended}にアイソスピンの項を加えると,
\begin{align}
  \Ham_{\pi^{\pm}N}^{\prime}
  = \frac{\sqrt{2}f}{m_\pi} \sum_{n}
  \qty[
    \tau_n^{(-)} \bm{\sigma}_n \cdot \grad \phi
    + \tau_n^{(+)} \bm{\sigma}_n \cdot \grad \phi^{\dagger}
  ]
\end{align}
が得られる.
$\tau_n^{(\pm)}$は$n$番目の核子のアイソスピンの昇降演算子$\tau^{(\pm)}$であり, $\sqrt{2}$は結果を簡単にするために乗じた.
\eqref{eq:phi_pipm}の$\phi$, $\phi^{\dagger}$をこの$\Ham_{\pi^{\pm}N}^{\prime}$に代入すると, $\Ham_{\pi^0N}^{\prime(2)}$のときと同じように計算できて,
\begin{align}
  \Ham_{\pi^{\pm}N}^{\prime}
  = i \frac{\sqrt{2}f}{m_\pi} \sum_{n} \sum_{k}
  \frac{1}{\sqrt{2 \omega_k V}}
  (\bm{\sigma}_n \cdot \bm{k})
  \qty[
    \qty(
    \tau_n^{(-)} \hat{b}_{k}
    + \tau_n^{(+)} \hat{c}_{-k}
    ) \exp(i \bm{k} \cdot \bm{r}_{n})
    - \qty(
    \tau_n^{(+)} \hat{b}_{k}^{\dagger}
    + \tau_n^{(-)} \hat{c}_{-k}^{\dagger}
    ) \exp(- i \bm{k} \cdot \bm{r}_{n})
  ]
\end{align}
となる.
これを元に, $\pi^0$メソンのときと同じように2個の核子間の$\pi^{\pm}$メソンの交換による核力ポテンシャル$U_{\pm}$を計算する.
$\pi^0$メソンのときと違って, $\pi^{\pm}$の2種類のメソンがあることに注意が必要であり,
$\pi^{+}$の状態を$\ket{k}^{+}$, $\pi^{-}$の状態を$\ket{k}^{-}$のように表すことにする.
\begin{figure}[tbp]
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/diagram-1-2-plus.png}
      \subcaption{}
    \end{center}
  \end{minipage}
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/diagram-1-2-minus.png}
      \subcaption{}
    \end{center}
  \end{minipage}
  \\
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/diagram-2-1-plus.png}
      \subcaption{}
    \end{center}
  \end{minipage}
  \begin{minipage}{0.5\hsize}
    \begin{center}
      \includegraphics[clip, width=60mm]{img/diagram-2-1-minus.png}
      \subcaption{}
    \end{center}
  \end{minipage}
  \caption{2核子間の$\pi^{\pm}$メソンの交換.}
  \label{fig:charged-pi-excahnge-diagrams}
\end{figure}
2核子間の$\pi^{\pm}$メソンの交換は図\ref{fig:charged-pi-excahnge-diagrams}に示すパターンが考えられるから, $U_{\pm}$は
\begin{align}
  \begin{split}
    U_{\pm}
    & = \underset{\mathrm{(a)}}{\uwave{
        \sum_{k} \frac{
          \melmel{\Vac}{\Vac}{\Ham_2^{\prime}}{k}{\Vac}
          \melmel{\Vac}{k}{\Ham_1^{\prime}}{\Vac}{\Vac}
        }{
          -\omega_k
        }
      }} \\
    &\hspace{2ex} + \underset{\mathrm{(b)}}{\uwave{
        \sum_{k} \frac{
          \melmel{\Vac}{\Vac}{\Ham_2^{\prime}}{\Vac}{-k}
          \melmel{-k}{\Vac}{\Ham_1^{\prime}}{\Vac}{\Vac}
        }{
          -\omega_k
        }
      }} \\
    & \hspace{2ex} + \underset{\mathrm{(c)}}{\uwave{
        \sum_{k} \frac{
          \melmel{\Vac}{\Vac}{\Ham_1^{\prime}}{k}{\Vac}
          \melmel{\Vac}{k}{\Ham_2^{\prime}}{\Vac}{\Vac}
        }{
          -\omega_k
        }
      }} \\
    & \hspace{2ex} + \underset{\mathrm{(d)}}{\uwave{
        \sum_{k} \frac{
          \melmel{\Vac}{\Vac}{\Ham_1^{\prime}}{\Vac}{-k}
          \melmel{-k}{\Vac}{\Ham_2^{\prime}}{\Vac}{\Vac}
        }{
          -\omega_k
        }
      }}
  \end{split}
  \label{eq:U_pm_mel}
\end{align}
と表せる.
$U_0$のときと同じように, $\Ham_{i}^{\prime}=\qty(\Ham_{\pi^{\pm}N}^{\prime})_{n=i}$である.
\eqref{eq:U_pm_mel}の項の下に付けた(a)などのラベルは, 図\ref{fig:charged-pi-excahnge-diagrams}のダイアグラムと対応している.
ここで, 核子1, 2が陽子であるか中性子であるかというのを気にしなければならないのではないかと思うかもしれない.
しかし, 陽子か中性子か判断し, $\pi^{\pm}$メソンを放出あるいは吸収させて状態を遷移させる役割は$\tau^{(\pm)}$に任せているので, (安心して)$U_0$のときと同じように$U_{\pm}$を計算することができる.
行列要素は前と同じように,
\begin{align}
  \melmel{\Vac}{k}{\Ham_1^{\prime}}{\Vac}{\Vac}
   & = {\vphantom{\bra{\Vac}}}^{-}\hspace{-.5ex}\bra{\Vac}
  {\vphantom{\bra{k}}}^{+}\hspace{-.5ex}\bra{k}
  i \frac{f}{m_\pi}
  \sum_{k^{\prime}}
  \frac{1}{\sqrt{\omega_k V}}
  (\bm{\sigma}_{1} \cdot \bm{k}^{\prime}) \notag                                                                    \\
   & \hspace{4ex} \qty[
    \qty(\tau_1^{(-)} \hat{b}_{k} + \tau_1^{(+)} \hat{c}_{-k}) \exp(i \bm{k}^{\prime} \cdot \bm{r}_1)
    - \qty(\tau_1^{(+)} \hat{b}_{k}^{\dagger} + \tau_1^{(-)} \hat{c}_{-k}^{\dagger}) \exp(- i \bm{k}^{\prime} \cdot \bm{r}_1)
  ]
  \ket{\Vac}^{+}
  \ket{\Vac}^{-} \notag                                                                                             \\
   & = -i\frac{f}{m_{\pi}\sqrt{\omega_k V}} (\bm{\sigma}_1 \cdot \bm{k}) \tau_1^{(+)} \exp(- i \bm{k}\cdot\bm{r}_1)
\end{align}
と計算できて,
\begin{align}
  U_{\pm}
   & = - \sum_{k}
  \frac{f^2}{m_{\pi}^2 \omega_k^2 V}
  (\bm{\sigma}_1 \cdot \bm{k})
  (\bm{\sigma}_2 \cdot \bm{k})
  \tau_1^{(+)} \tau_2^{(-)}
  \exp(i\bm{k} \cdot (\bm{r}_2 - \bm{r}_1))
  - \sum_{k}
  \frac{f^2}{m_{\pi}^2 \omega_k^2 V}
  (\bm{\sigma}_1 \cdot \bm{k})
  (\bm{\sigma}_2 \cdot \bm{k})
  \tau_1^{(-)} \tau_2^{(+)}
  \exp(i\bm{k} \cdot (\bm{r}_2 - \bm{r}_1)) \notag   \\
   & \hspace{2ex} - \sum_{k}
  \frac{f^2}{m_{\pi}^2 \omega_k^2 V}
  (\bm{\sigma}_1 \cdot \bm{k})
  (\bm{\sigma}_2 \cdot \bm{k})
  \tau_1^{(-)} \tau_2^{(+)}
  \exp(- i\bm{k} \cdot (\bm{r}_2 - \bm{r}_1))
  - \sum_{k}
  \frac{f^2}{m_{\pi}^2 \omega_k^2 V}
  (\bm{\sigma}_1 \cdot \bm{k})
  (\bm{\sigma}_2 \cdot \bm{k})
  \tau_1^{(+)} \tau_2^{(-)}
  \exp(- i\bm{k} \cdot (\bm{r}_2 - \bm{r}_1)) \notag \\
   & = -2 \qty(
  \tau_1^{(+)} \tau_2^{(-)}
  + \tau_1^{(-)} \tau_2^{(+)}
  )
  \sum_{k}
  \frac{f^2}{m_\pi^2 \omega_k^2 V}
  (\bm{\sigma}_1 \cdot \bm{k})
  (\bm{\sigma}_2 \cdot \bm{k})
  \cos(\bm{k}\cdot(\bm{r}_1 - \bm{r}_2)) \notag      \\
   & = -2 \qty(
  \tau_1^{(+)} \tau_2^{(-)}
  + \tau_1^{(-)} \tau_2^{(+)}
  )
  \frac{f^2}{(2\pi)^3 m_\pi^2}
  \int \!\!\! \dd[3]{k}
  \frac{
    (\bm{\sigma}_1 \cdot \bm{k})
    (\bm{\sigma}_2 \cdot \bm{k})
    \cos(\bm{k}\cdot(\bm{r}_1 - \bm{r}_2))
  }{
    k^2 + m_\pi^2
  }
\end{align}
を得る.
これは\eqref{eq:U0_2_int}に
\begin{align}
  2\qty(
  \tau_1^{(+)} \tau_2^{(-)}
  + \tau_1^{(-)} \tau_2^{(+)}
  )
  = \tau_{1x}\tau_{2x} + \tau_{1y}\tau_{2y}
\end{align}
を乗じただけのものであるから, $U_0$の計算の結果を使うことができて, $U_{\pm}$は
\begin{align}
  U_{\pm}
  = \qty(\tau_{1x}\tau_{2x} + \tau_{1y}\tau_{2y})
  \frac{1}{3}\frac{f^2}{4\pi} \qty[
    (\bm{\sigma_1}\cdot\bm{\sigma_2})
    + \qty(
    1 + \frac{3}{m_{\pi}r} + \frac{3}{(m_{\pi}r)^2}
    ) S_{1,2}
  ] \frac{\exp(-m_{\pi}r)}{r}
  \label{eq:U_pm}
\end{align}
となる.
\section{場の量子化}\label{sec:QFT}
$\pi$メソンの交換によって核力を説明するためには場の量子論の知識を必要とする.
$\pi$メソンによる核力の詳細をみていく前に, 後で使う事項と式について軽く説明する.

\subsection{粒子と場の対応関係}\label{sec:QFT:fields}
粒子と場の関係は表\ref{tab:particle-field}のようになっている.
今回扱う$\pi^{0}$, $\pi^{\pm}$メソンは中性スピン0ボソン, 荷電スピン0ボソンであるから, それぞれ実スカラー場と複素スカラー場が対応する.
こういう関係があるのだと言い切ってしまえばそれまでだが, なんだかそれではゼミ発表担当としての責任を果たしていない気がするので, 表\ref{tab:particle-field}の上2つの対応を示す.

\begin{table}[htp]
  \caption{粒子と場の対応関係}
  \begin{center}
    \begin{tabular}{c c} \hline \hline
      粒子                                             & 場                               \\ \hline
      スピン0ボソン                                    & 実スカラー場 $\phi(\bm{x},t)$    \\
      荷電スピン0ボソン                                & 複素スカラー場 $\phi(\bm{x},t)$  \\
      スピン1質量0ボソン(光子)                         & 実ベクトル場 $A_{\mu}(\bm{x},t)$ \\
      スピン$1/2$フェルミオン($\e^{\pm}$, クォークetc) & スピノール場 $\psi_r(\bm{x}, t)$ \\ \hline \hline
    \end{tabular}
  \end{center}
  \label{tab:particle-field}
\end{table}

\subsection{中性スピン0ボソン}\label{sec:QFT:meson0}
\subsubsection{中性スピン0ボソンに対応する場}
運動量$\bm{k}$のボソンの波動関数$u_k(x)$は, Klein-Gordon方程式:
\begin{align}
  (\Box + \mu^2) u_k(x) = 0
  \label{eq:KGEq}
\end{align}
に従う.
ここで, $\Box$はダランベルシアン
\begin{align}
  \Box
  = \frac{1}{c^2}\pdv{t} - \laplacian
\end{align}
$\mu$は
\begin{align}
  \mu = \frac{mc}{\hbar}
\end{align}
である.
粒子が体積$V=L^3$の立方体中にあるとすると, \eqref{eq:KGEq}の解$u_k(x)$は
\begin{align}
  u_k(x)
  = \frac{1}{\sqrt{2\omega_k V}} \exp(i\bm{k}\cdot\bm{x})
\end{align}
である.
ここで, $\omega_k$, $\bm{x}$, $\bm{k}$は,
\begin{align}
  \omega_k
   & = c\sqrt{\bm{k}^2 + \mu^2}                \\
  \bm{x}
   & = \qty(ct, x_1, x_2, x_3)                 \\
  \bm{k}
   & = \qty(\frac{\omega_k}{c}, k_1, k_2, k_3)
\end{align}
であり, $u_k(x)$を\eqref{eq:KGEq}に代入すると, 解となっていることは容易に確認できる.
以降, 自然単位系$c=\hbar=1$で計算を進める.

$\ket{\Vac}$で粒子が1つも存在しない真空状態を表し, 運動量$\bm{k}$の粒子が$n$個ある状態を$\ket{n_{k}}$と表すことにする.
また, 運動量が$\bm{k}_1$, $\bm{k}_2$の粒子が$n_{k_1}$, $n_{k_2}$個ある状態を$\ket{n_{k_1}, n_{k_2}}$のように表す.

次に, 運動量$\bm{k}_i$のボソンを生成, 消滅させる演算子$\hat{a}_{k_i}^{\dagger}$, $\hat{a}_{k_i}$を定義する.
$\hat{a}_{k_i}^{\dagger}$, $\hat{a}_{k_i}$は交換関係
\begin{align}
  \begin{split}
    [\hat{a}_{k_i}, \hat{a}_{k_j}^{\dagger}] &= \delta_{k_i,k_j} \\
    [\hat{a}_{k_i}, \hat{a}_{k_j}] &= 0 \\
    [\hat{a}_{k_i}^{\dagger}, \hat{a}_{k_j}^{\dagger}] &= 0
  \end{split}
\end{align}
と真空の定義
\begin{align}
  \hat{a}_{k_i}\ket{\Vac} = 0, \quad \braket{\Vac} = 1 .
\end{align}
をみたすものとして定義することができる.
生成, 消滅演算子の働きをわかりやすいように書けば, 次のようになる.
\begin{align}
  \hat{a}_{k_i}^{\dagger} \ket{n_{k_1}, n_{k_2}, \cdots , n_{k_i}, \cdots}
   & = \sqrt{n_{k_i}+1} \ket{n_{k_1}, n_{k_2}, \cdots , n_{k_i}+1, \cdots}                 \\
  \hat{a}_{k_i} \ket{n_{k_1}, n_{k_2}, \cdots , n_{k_i}, \cdots}
   & = \sqrt{n_{k_i}} \ket{n_{k_1}, n_{k_2}, \cdots , n_{k_i}-1, \cdots} \quad (n_{k_i}>0)
\end{align}
この$\hat{a}_{k_i}^{\dagger}$, $\hat{a}_{k_i}$を使って, 場の演算子$\phi(x)$は,
\begin{align}
  \begin{split}
    \phi(x)
    & = \sum_{k} \qty(
    \hat{a}_{k} u_k(x)
    + \hat{a}_{k}^{\dagger} u_k^{\ast}(x)
    ) \\
    & = \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    \hat{a}_{k} \exp(i\bm{k}\cdot\bm{x})
    + \hat{a}_{k}^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
    )
  \end{split}
  \label{eq:meson0phi}
\end{align}
と定義できる\footnote{この$\phi(x)$の物理的な意味は, $\bm{x}$に$\bm{k}$の粒子の生成と消滅の両方を行うということになる.}.
\eqref{eq:meson0phi}を見れば分かるように$\phi(x)$もKlein-Gordon方程式\eqref{eq:KGEq}をみたす.
\begin{align}
  (\Box + m^2) \phi(x) = 0
\end{align}
$\phi(x)$はエルミートな演算子($\phi=\phi^{\dagger}$)であるので, 古典的には($\hat{a}_k$をただの複素数とみなして)実スカラー場と対応する.

\subsubsection{中性自由スカラー場のHamiltonian}
この自由スカラー場のLagrangian $\Lag$は
\begin{itemize}
  \item Lorentz不変である.
  \item Lagrangianから導かれるEOMがKlein-Gordon方程式となる.
\end{itemize}
という要請から,
\begin{align}
  \Lag
  = \frac{1}{2} \qty(
  \partial_\nu \phi \partial^\nu \phi
  - m^2 \phi^2
  )
  \label{eq:Lphi0}
\end{align}
となる.
実際, \eqref{eq:Lphi0}をEuler-Lagrange方程式に代入してみると,
\begin{align}
  \pdv{\Lag}{\phi}
  - \partial_{\nu} \pdv{\Lag}{(\partial_{\nu}\phi)}
   & = -m^2 \phi
  - \frac{1}{2} \partial_\nu \pdv{(\partial_\nu \phi)}\qty[
    \qty(\pdv{\phi}{t})^2
    - \laplacian{\phi}
  ] \notag                    \\
   & = - (\Box + m^2) \phi(x)
  = 0
\end{align}
となり, Klein-Gordon方程式をみたすことが分かる.
$\varphi(x)$に対する正準共役運動量$\pi(x)$は
\begin{align}
  \pi(x)
  = \pdv{\Lag}{(\partial_0 \phi)}
  = \partial_0 \phi(x)
\end{align}
であるから, Hamiltonian $\Ham$は
\begin{align}
  \Ham
  =  \int \! \dd[3]{x} \frac{1}{2} \qty[\pi^2 + (\grad \phi)^2 + m^2 \phi^2]
\end{align}
となる.
これに\eqref{eq:meson0phi}を代入して,
\begin{align}
  \int \! \dd[3]{x} \exp(i \bm{k} \cdot \bm{x}) \exp(- i \bm{k}^{\prime} \cdot \bm{x})
  = \delta(\bm{k} - \bm{k}^{\prime})
\end{align}
に注意して計算すると,
\begin{align}
  \Ham
  = \frac{1}{2} \sum_{k} \omega_{k} \qty(\hat{a}_{k}\hat{a}_{k}^{\dagger} + \hat{a}_{k}^{\dagger}\hat{a}_{k})
\end{align}
$[\hat{a}_{k}, \hat{a}_{k}^{\dagger}] = 1$より, $\hat{a}_{k}\hat{a}_{k}^{\dagger} = 1 + \hat{a}_{k}^{\dagger}\hat{a}_{k}$だから, $\Ham$は
\begin{align}
  \Ham
  = \sum_{k} \omega_{k} \qty(\hat{a}_{k}^{\dagger}\hat{a}_{k} + \frac{1}{2})
  = \sum_{k} \omega_{k} \qty(\hat{\mathcal{N}} + \frac{1}{2})
  \label{eq:Ham0VE}
\end{align}
と書ける.
$\hat{\mathcal{N}}$は数演算子$\hat{a}_{k}^{\dagger}\hat{a}_{k}$であり, $\bm{k}$のメソンの数を数えることに対応する演算子である.
\eqref{eq:Ham0VE}の定数項$\omega_k/2$は真空状態のエネルギー\footnote{
  真空エネルギー$\sum_{k}\omega_k$は量子化において演算子の順序が一意的でないために生じる.
  つまり, 実スカラー場$\varphi$を場の演算子$\phi$で置き換える際に, $\hat{a}_k$, $\hat{a}_k^\dagger$の順序を指定しないために生じてしまう.
  古典的には$\hat{a}_k^\dagger\hat{a}_k$と$\frac{1}{2}(\hat{a}_k^\dagger\hat{a}_k + \hat{a}_k\hat{a}_k^{\dagger})$は同じ意味になる.
  しかし, 量子的には真空エネルギーが0となる, つまり正しい演算子の形は$\hat{a}_k\hat{a}_k^\dagger$であり, $\frac{1}{2}(\hat{a}_k^\dagger\hat{a}_k + \hat{a}_k\hat{a}_k^{\dagger})$は正しい形ではない.
}であるので, これを省略して,
\begin{align}
  \Ham
  = \sum_{k} \omega_{k} \hat{a}_{k}^{\dagger}\hat{a}_{k}
  = \sum_{k} \omega_{k} \hat{\mathcal{N}}
  \label{eq:Ham0}
\end{align}
と書く.
\eqref{eq:Ham0}は, 運動量$\bm{k}$のメソン1つのエネルギーは$\omega_k=\sqrt{\bm{k}^2+m^2}$であり, 系全体のエネルギーは$\omega_k$を粒子数の分だけ足し上げていったものということを意味している.
これは直観的にも理解しやすい.

\subsection{荷電スピン0ボソン}\label{sec:QFT:mesonpm}
\ref{sec:QFT:meson0}節では, 中性スピン0ボソンは実スカラー場と対応することを説明し, 自由実スカラー場のHamiltonianを導いた.
次に興味があるのは, 中性ではなく電荷を持つボソンがどのような場と対応するかということである.
この節では, 荷電スピン0ボソンが複素スカラー場と対応することを説明する.

\subsubsection{複素スカラー場}
実スカラー場$\varphi_{\Real}$は
\begin{align}
  \varphi_{\Real}(x)
  = \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
  a_k \exp(i\bm{k}\cdot\bm{x})
  + a_k^{\ast} \exp(-i \bm{k}\cdot\bm{x})
  )
  \label{eq:real_varphi}
\end{align}
と表せた.
これを複素に拡張し, 複素スカラー場$\varphi_{\Comp}$
\begin{align}
  \begin{split}
    \varphi_{\Comp}(x)
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    b_k \exp(i\bm{k}\cdot\bm{x})
    + c_{-k}^{\ast} \exp(-i\bm{k}\cdot\bm{x})
    ) \\
    \varphi_{\Comp}^{\ast}(x)
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    b_k^{\ast} \exp(-i\bm{k}\cdot\bm{x})
    + c_{-k} \exp(i\bm{k}\cdot\bm{x})
    )
  \end{split}
  \label{eq:comp_varphi}
\end{align}
を考える.
$a_k \exp(i\bm{k}\cdot\bm{x})$と$a_k^{\ast} \exp(-i \bm{k}\cdot\bm{x})$が複素共役であるので, $\varphi_{\Real}$は実数となる.
一方, \eqref{eq:comp_varphi}を見て分かるように, $b_k \exp(i\bm{k}\cdot\bm{x})$と$c_{-k}^{\ast} \exp(-i\bm{k}\cdot\bm{x})$は複素共役になっていないので, $\varphi_{\Comp}$は複素数となる.
この$b_k$, $b_k^{\ast}$, $c_{-k}$, $c_{-k}^{\ast}$を生成, 消滅演算子$\hat{b}_{k}$, $\hat{b}_{k}^{\dagger}$, $\hat{c}_{-k}$, $\hat{c}_{-k}^{\dagger}$に置き換えて, 場の演算子を構成する.
\begin{align}
  \begin{split}
    \phi(x)
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    \hat{b}_k \exp(i\bm{k}\cdot\bm{x})
    + \hat{c}_{-k}^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
    ) \\
    \phi^{\dagger}(x)
    &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
    \hat{b}_k^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
    + \hat{c}_{-k} \exp(i\bm{k}\cdot\bm{x})
    )
  \end{split}
  \label{eq:phicomp}
\end{align}
$\hat{b}_{k}$, $\hat{c}_{-k}$の交換関係は次のようである.
\begin{align}
  \begin{split}
    [\hat{b}_{k_i}, \hat{b}_{k_j}^{\dagger}]
    &= [\hat{c}_{-k_i}, \hat{c}_{-k_j}^{\dagger}]
    = \delta_{k_i,k_j} \\
    [\hat{b}_{k_i}, \hat{b}_{k_j}]
    &= [\hat{c}_{-k_i}, \hat{c}_{-k_j}]
    = 0 \\
    [\hat{b}_{k_i}^{\dagger}, \hat{b}_{k_j}^{\dagger}]
    &= [\hat{c}_{-k_i}^{\dagger}, \hat{c}_{-k_j}^{\dagger}]
    = 0 \\
    [\hat{b}_{k_i}, \hat{c}_{-k_j}]
    &= [\hat{b}_{k_i}^{\dagger}, \hat{c}_{-k_j}^{\dagger}]
    = [\hat{b}_{k_i}, \hat{c}_{-k_j}^{\dagger}]
    = [\hat{b}_{k_i}^{\dagger}, \hat{c}_{-k_j}]
    = 0
  \end{split}
\end{align}
実スカラー場のときと同じように
\begin{itemize}
  \item Lorentz不変である.
  \item Lagrangianから導かれるEOMがKlein-Gordon方程式となる.
\end{itemize}
という要請から, Lagrangian $\Lag$を考えると, 自由複素スカラー場の$\Lag$は,
\begin{align}
  \Lag
  = \partial_{\nu} \phi^{\dagger} \partial^{\nu} \phi - m^2 \phi^{\dagger}\phi
  \label{eq:Lphicomp}
\end{align}
となる.
実際にこれをEuler-Lagrange方程式に代入してみると,
\begin{align}
  (\Box - m^2) \phi^{\dagger} = (\Box - m^2) \phi = 0
\end{align}
となり, Klein-Gordon方程式が得られる.

複素スカラー場の変換
\begin{align}
  \begin{split}
    \phi &\to \phi^{\prime} = \exp(-i\alpha) \phi \\
    \phi^{\dagger} &\to \phi^{\prime \dagger} = \exp(i\alpha)\phi^{\dagger}
  \end{split}
  \label{eq:GlobalSym}
\end{align}
を考えると, この変換のもとで$\Lag$は不変であることが分かる.
つまり, 作用$S$もこの変換に対して不変であり, ネーターの定理から保存量が存在することがわかる.
$\alpha$として微少量ととると, 変換\eqref{eq:GlobalSym}は次のように表せる.
\begin{align}
  \begin{split}
    \phi
    &\to \phi^{\prime}
    = (1-i\alpha)\phi
    = \phi - i\alpha\phi \\
    \phi^{\dagger}
    &\to \phi^{\prime \dagger}
    = (1+i\alpha)\phi^{\dagger}
    = \phi + i\alpha\phi^{\dagger} \\
  \end{split}
\end{align}
つまり,
\begin{align}
  \delta \phi = -i\alpha\phi,
  \quad \delta \phi^\dagger = +i\alpha\phi .
\end{align}
である.
これからカレント$J^{\mu}$は
\begin{align}
  J^{\mu}
  = -i \qty[
    \phi^{\dagger} (\partial^{\mu}\phi)
    - (\partial^{\mu}\phi^{\dagger}) \phi
  ]
\end{align}
と計算でき, 保存量$Q$は
\begin{align}
  Q
   & = \int\!\dd[3]{x} J^{0} \notag \notag \\
   & = -i \int\!\dd[3]{x} \qty(
  \phi^{\dagger}(\partial^0\phi)
  - (\partial^{0}\phi^{\dagger})\phi
  ) \notag                                 \\
   & = \sum_{k} \qty[
    \hat{b}_{k}^{\dagger} \hat{b}_{k}
    - \hat{c}_{-k}^{\dagger} \hat{c}_{-k}
  ] \notag                                 \\
   & = \sum_{k} \qty[
    \hat{\mathcal{N}}_{k}^{(+)}
    - \hat{\mathcal{N}}_{k}^{(-)}
  ]
  (= N^{(+)} - N^{(-)})
  \label{eq:Q_phicomp}
\end{align}
となる.
$J^{\mu}$を4元電流とみなせば, $J^{0}$は電荷密度であり, その全空間積分である$Q$は電荷に相当する.
つまり, \eqref{eq:Q_phicomp}は電荷の保存則を表している.
$\hat{b}$によって生成, 消滅される粒子の電荷を$+$ととると, $\hat{c}$によって生成, 消滅される粒子の電荷は$-$となり,
複素スカラー場$\phi$は電荷をもつ粒子を記述することができる.
なお, 変換\eqref{eq:GlobalSym}を大域的位相変換と呼ぶ.

\subsubsection{荷電自由スカラー場のHamiltonian}
荷電スピン0ボソンは複素スカラー場と対応することは説明できた.
あとは, この荷電自由スカラー場のHamiltonianを求めれば, 準備としては万全である.
Lagrangianの表式も既に導いているし, Hamiltonianの計算も実スカラー場のときとほとんど同じようにしてできる(大体$\delta$関数の計算)ので, 細かな計算は省略させてもらう.
Hamiltonian $\Ham$は
\begin{align}
  \Ham
  = \sum_{k} \omega_{k} \qty(
  \hat{b}_{k}^{\dagger}\hat{b}_{k}
  + \hat{c}_{-k}^{\dagger}\hat{c}_{-k}
  )
  = \sum_{k} \omega_{k} \qty(
  \hat{N}_{k}^{(+)}
  + \hat{N}_{k}^{(-)}
  )
  \label{eq:Ham_phicomp}
\end{align}
となる.
これは正負の電荷をもつ粒子を数え上げ, その粒子のエネルギー$\omega_k$を足し上げることを意味している.
これは中性スカラー場の粒子の種類を2つにしただけなので, 直観的に理解できる.

\begin{tcolorbox}[
    title=ネーターの定理,
    breakable,
    colback=black!2!white
  ]
  $\phi$の変分$\delta\phi$を考える.
  この変分による作用$S$の変化$\delta S$は
  \begin{align}
    \begin{split}
      \delta S
      &= \int \! \dd[4]{x} \qty[
        \pdv{\Lag}{\phi}\delta\phi + \pdv{\Lag}{(\partial_{\mu})\phi} \delta(\partial_\mu \phi)
      ] \\
      &= \int \dd[4]{x} \qty[
        \partial_{\mu} \qty(
        \pdv{\Lag}{(\partial_{\mu}\phi)}
        )\delta\phi
        + \pdv{\Lag}{(\partial_{\mu}\phi)} \partial_{\mu} (\delta \phi)
      ] \\
      &= \int \! \dd[4]{x} \partial_{\mu} \qty[
        \pdv{\Lag}{(\partial_{\mu}\phi)}\delta\phi
      ]
    \end{split}
  \end{align}
  となる.
  これから, 変分$\delta\phi$に対して作用$S$が不変に保たれるならば,
  \begin{align}
    \partial_{\mu} \qty[
      \pdv{\Lag}{(\partial_{\mu}\phi)}\delta\phi
    ]
    = \partial_{\mu} J^{\mu}
    = 0
    \label{eq:Noether}
  \end{align}
  が成り立つことが分かる.
  $J^{\mu}=\pdv{\Lag}{(\partial_{\mu}\phi)}\delta\phi$を4元カレントと呼ぶ.
  \begin{align}
    Q = \int \! \dd[3]{x} J^{0}
  \end{align}
  を定義すると, \eqref{eq:Noether}から,
  \begin{align}
    \dv{Q}{t}
    = \int \! \dd[3]{x} \pdv{J^0}{t}
    = - \int \! \dd[3]{x} \sum_{i=1}^{3} \partial_i J^i
    = - \int \! \dd{\bm{S}} \cdot \bm{J}
    \quad (\because \text{Gaussの発散定理})
  \end{align}
  を得る.
  場が十分速く無限遠方で0になるとすると, この表面項は0となるから,
  \begin{align}
    \dv{Q}{t} = 0
  \end{align}
  となる.
  つまり, $Q$は時間によらない保存量である.
  代表的な対称性と保存量の対応を表\ref{tab:Noether}に示す.
  \begin{table}[H]
    \caption{対称性と保存量の対応}
    \begin{center}
      \begin{tabular}{c c} \hline \hline
        対称性         & 保存量     \\ \hline
        時間の平行移動 & エネルギー \\
        空間の平行移動 & 運動量     \\
        空間回転       & 角運動量   \\
        大域的位相変換 & 電荷       \\ \hline \hline
      \end{tabular}
    \end{center}
    \label{tab:Noether}
  \end{table}
\end{tcolorbox}

\subsection{まとめ}
最後に後で使う事項, 式をまとめておく.
\begin{tcolorbox}[
    title=スピン0ボソンと対応する場,
    breakable,
    colback=black!2!white
  ]
  \begin{itemize}
    \item 中性スピン0ボソンは実スカラー場, 荷電スピン0ボソンは複素スカラー場と対応する.
    \item 中性スカラー場は
          \begin{align}
            \phi(x)
            = \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
            \hat{a}_{k} \exp(i\bm{k}\cdot\bm{x})
            + \hat{a}_{k}^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
            ) .
            \tag{\ref{eq:meson0phi}再}
          \end{align}
    \item 自由中性スカラー場のHamiltonianは
          \begin{align}
            \Ham
            = \sum_{k} \omega_k \hat{a}_k^\dagger \hat{a}_k
            = \sum_{k} \omega_k \hat{\mathcal{N}} .
            \tag{\ref{eq:Ham0}再}
          \end{align}
    \item 荷電スカラー場は
          \begin{align}
            \begin{split}
              \phi(x)
              &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
              \hat{b}_k \exp(i\bm{k}\cdot\bm{x})
              + \hat{c}_{-k}^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
              ) . \\
              \phi^{\dagger}(x)
              &= \sum_{k} \frac{1}{\sqrt{2 \omega_k V}} \qty(
              \hat{b}_k^{\dagger} \exp(-i\bm{k}\cdot\bm{x})
              + \hat{c}_{-k} \exp(i\bm{k}\cdot\bm{x})
              ) .
            \end{split}
            \tag{\ref{eq:phicomp}再}
          \end{align}
    \item 自由荷電スカラー場のHamiltonianは
          \begin{align}
            \Ham
            = \sum_{k} \omega_{k} \qty(
            \hat{b}_{k}^{\dagger}\hat{b}_{k}
            + \hat{c}_{-k}^{\dagger}\hat{c}_{-k}
            )
            = \sum_{k} \omega_{k} \qty(
            \hat{N}_{k}^{(+)}
            + \hat{N}_{k}^{(-)}
            ) .
            \tag{\ref{eq:Ham_phicomp}再}
          \end{align}
  \end{itemize}
\end{tcolorbox}